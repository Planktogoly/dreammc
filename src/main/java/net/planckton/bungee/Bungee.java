package net.planckton.bungee;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.plugin.Plugin;
import net.planckton.bungee.commands.KickCommand;
import net.planckton.bungee.commands.SendCommand;
import net.planckton.bungee.commands.SetMOTDCommand;
import net.planckton.bungee.listener.ProxyListener;

public class Bungee extends Plugin {
	
	@Getter private static Bungee instance;
	
	@Getter @Setter private String motd = "none motd set";
	
    @Override
    public void onEnable() {
    	instance = this;
    	
    	getProxy().getPluginManager().registerCommand(this, new KickCommand("kick"));
    	getProxy().getPluginManager().registerCommand(this, new SetMOTDCommand("setmotd"));
    	getProxy().getPluginManager().registerCommand(this, new SendCommand("ssend"));
    	getProxy().getPluginManager().registerListener(this, new ProxyListener());
    }
    
    @Override
    public void onDisable() {
    	instance =  null;
    }

}
