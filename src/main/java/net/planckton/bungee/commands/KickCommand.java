package net.planckton.bungee.commands;


import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class KickCommand extends Command {

	public KickCommand(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (!(sender instanceof ProxiedPlayer)) return;
		ProxiedPlayer player = (ProxiedPlayer) sender;
		
		if (args.length < 1) {
			player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Usage: /kick <player|server|all> <reason>"));
			return;
		}
		
		if (args.length >= 1) {
			StringBuilder builder = new StringBuilder();
			
			for (String string : args) {
				if (string.equalsIgnoreCase(args[0])) continue;
				
				builder.append(string + " ");
			}
			
			if (args[0].equalsIgnoreCase("all")) {
				ProxyServer.getInstance().getPlayers().forEach(proxiedPlayer -> 
				proxiedPlayer.disconnect(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', builder.toString()))));
				return;
			} else if (isServerName(args[0])) {
				for (ServerInfo server : ProxyServer.getInstance().getServers().values()) {
					if (args[0].equalsIgnoreCase(server.getName())) {
						server.getPlayers().forEach(proxiedPlayer -> proxiedPlayer.disconnect(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', builder.toString()))));
					}
				}
			} else {
				ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(args[0]);
				if (proxiedPlayer == null) {
					player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "This is player is not online!"));
					return;
				}
				
				proxiedPlayer.disconnect(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', builder.toString())));
			}
		}
 
	}
	private boolean isServerName(String name) {
		for (ServerInfo server : ProxyServer.getInstance().getServers().values()) {
			if (name.equalsIgnoreCase(server.getName())) return true;
		}
		return false;
	}

}
