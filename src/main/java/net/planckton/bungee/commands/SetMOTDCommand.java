package net.planckton.bungee.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.planckton.bungee.Bungee;

public class SetMOTDCommand extends Command {

	public SetMOTDCommand(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (!(sender instanceof ProxiedPlayer)) return;
		ProxiedPlayer player = (ProxiedPlayer) sender;
		
		if (args.length == 0) {
			player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Usage: /setmotd <motd>"));
			return;
		}
		
		StringBuilder builder = new StringBuilder();
		
		for (String string : args) {
			builder.append(string + " ");
		}
		
		Bungee.getInstance().setMotd(builder.toString());
		player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "You have set the motd to " + builder));
		return;
	}

}
