package net.planckton.bungee.listener;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.planckton.bungee.Bungee;

public class ProxyListener implements Listener {
	
	@EventHandler
	public void onProxyPing(ProxyPingEvent event) {
		ServerPing ping = event.getResponse();
		
		ping.setDescriptionComponent(new TextComponent(ChatColor.translateAlternateColorCodes('&', Bungee.getInstance().getMotd())));		
		//new TextComponent(ChatColor.translateAlternateColorCodes('&', Bungee.getInstance().getMotd()))
	}
}
